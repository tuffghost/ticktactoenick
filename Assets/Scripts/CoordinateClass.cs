﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoordinateClass
{

    public class CoordinateClassObject 
    {
        private int locationX;
        private int locationY;

        public int LocationX
        {

            get { return locationX; }

            set { locationX = value; }
        }

        public int LocationY {

        get { return locationY; }

        set { locationY = value; }
}
    }
}
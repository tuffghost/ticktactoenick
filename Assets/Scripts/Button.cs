﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CoordinateClass;

public class Button : MonoBehaviour
{
    public UnityEngine.UI.Button CurrentButton;
    private CoordinateClassObject ButtonLocation = new CoordinateClassObject();

    
    [SerializeField]
    int btnLocationX = 0;
    [SerializeField]
    int btnLocationY = 0;

    // Start is called before the first frame update
    void Start()
    {
        CurrentButton.GetComponentInChildren<Text>().text = "";
        CurrentButton.onClick.AddListener(TaskOnClick);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void TaskOnClick()
    {
        ButtonLocation.LocationX = btnLocationX;
        ButtonLocation.LocationY = btnLocationY;
        Debug.Log(btnLocationX + "" + btnLocationY);
        Debug.Log(ButtonLocation);
        Debug.Log(GameManager.instance.Player1turn);
        if (GameManager.instance.Player1turn)
        {
            CurrentButton.GetComponentInChildren<Text>().text = "X";
            Debug.Log("player 2 turn");
            GameManager.instance.Player1turn = false;
            GameManager.instance.Player1moves.Add(ButtonLocation);
            Debug.Log(GameManager.instance.Player1turn);
        } else
        {
            CurrentButton.GetComponentInChildren<Text>().text = "O";
            GameManager.instance.Player1turn = true;
            GameManager.instance.Player2moves.Add(ButtonLocation);
            
        }
    }
}

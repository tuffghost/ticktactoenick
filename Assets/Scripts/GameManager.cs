﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CoordinateClass;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public bool Player1turn = true;
    public  List<CoordinateClassObject> Player1moves;
    public  List<CoordinateClassObject> Player2moves;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
